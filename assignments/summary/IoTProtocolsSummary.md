**IIOT PROTOCOLS**

IoT communication protocols are modes of communication that protect and ensure optimum security to the data being exchanged between connected devices. 

**1. 4-20mA CURRENT LOOP**

- The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems. In a current loop, the current signal is drawn from a dc power supply, flows through the transmitter, into the controller and then back to the power supply in a series circuit.
- The below picture shows components of 4-20mA current loop.

![](images/a18.jpg)

- **Advantages**
1. It can be run over long distances with minimal signal losses compared to voltage type signals
2. A varying current loop load impedance or supply voltage will not significantly affect the signal as long as it does not exceed recommended component limits
3. Rugged signal with low electromagnetic susceptibility
4. Saves on cable wire because it only needs 2 wires to function
5. Live zero reading verifies sensor is electrically functional

- **Disadvantages**
1. High power consumption compared to other analogue signal types
2. Elevated output at zero reading
3. Supply not isolated from output
4. Increasing circuit load resistance, will reduce the supply voltage available to power the transmitter that is generating the 4-20mA signal.

**2. MODBUS PROTOCOL**

- MODBUS Protocol is a messaging structure, widely used to establish master-slave communication between intelligent devices.
- Since Modbus protocol is just a messaging structure, it is independent of the underlying physical layer. It is traditionally implemented using RS232, RS422, or RS485.
- In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.

![](images/a19.jpg)

**HOW DOES MODBUS PROTOCOL WORKS?**

- Communication between a master and a slave occurs in a frame that indicates a function code.
- The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
- The slave then responds, based on the function code received.
- The protocol is commonly used in IoT as a local interface to manage devices.
- Modbus protocol can be used over 2 interfaces. They are -

1. RS485 - called as Modbus RTU - RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
2. Ethernet - called as Modbus TCP/IP - Ethernet protocol is a typical LAN technology.

**3. OPCUA PROTOCOL**

- OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation.
- It is one of the most important communication protocols for Industry 4.0 and the IoT.
- With OPC, access to machines, devices and other systems in the industrial environment is standardized and enables similar and manufacturer-independent data exchange.

- The picture below explains the OPU CA client and server model.

![](images/a20.png)

**4. CLOUD PROTOCOLS (MQTT AND HTTP)**

- Cloud IoT Core supports two protocols for device connection and communication: MQTT and HTTP. Devices communicate with Cloud IoT Core across a "bridge" — either the **MQTT** bridge or the **HTTP** bridge.
- The MQTT/HTTP bridge is a central component of Cloud IoT Core, as shown in the components overview.

- MQTT is a standard publish/subscribe protocol that is frequently used and supported by embedded devices, and is also common in machine-to-machine interactions.

<img src="images/a22.png" width=500>

- HTTP is a "connectionless" protocol: with the HTTP bridge, devices do not maintain a connection to Cloud IoT Core. Instead, they send requests and receive responses. Cloud IoT Core supports HTTP 1.1 only (not 2.0)

![](images/a23.jpeg)

Below is the difference between MQTT AND HTTP

<img src="images/a21.png" width=1000>

**REQUEST**

- Request in HTTP contains 3 parts - request line, HTTP Headers, Message Body.
- **GET request** - is a type of HTTP request using the GET method
- There are many different methods in the request. They are - 

- **GET** : Retrieve the resource from the server (e.g. when visiting a page)
- **POST** : Create a resource on the server (e.g. when submitting a form)
- **PUT/PATCH** : Update the resource on the server (used by APIs)
- **DELETE** : Delete the resource from the server (used by APIs)

**RESPONSE**

- It is comprised of 3 parts - Status line, HTTP Header, Message Body.
- Depending on the situation, the server sends different code.

![](images/a24.jpg)









