**EMBEDDED SYSTEMS**

An embedded system is a microprocessor-based computer hardware system with software that is designed to perform a dedicated function, either as an independent system or as a part of a large system. At the core is an integrated circuit designed to carry out computation for real-time operations.The systems can be programmable or with fixed functionality. The complexity of an embedded system varies significantly depending on the task for which it is designed. Typically, IoT devices are embedded systems. In IoT, the devices which have been embedded with electronics, internet connectivity and other forms of hardware like sensors can communicate and interact with other devices over the internet.

![](images/a1.jpg)

**EMBEDDED SYSTEMS FOUND IN MOST ELECTRONICS DEVICES**

Embedded systems are found in many devices in common use today. The capabilities provided by embedded systems enable electronic equipment to have far greater capabilities than would be possible if only hardware techniques were used. As a result, embedded systems are found in all manner of electronic equipment and gadgets.

![](images/a2.jpg)

Below are the basic electronic devices used in embedded systems.

**1. SENSORS AND ACTUATORS**

The intelligence and value from an IoT system is based on what can be learned from the data. Sensors are the source of IoT data. A better term for a sensor is a **transducer**. A transducer is any physical device that converts one form of energy into another. So, in the case of a sensor, the transducer converts some physical phenomenon into an electrical impulse that can then be interpreted to determine a reading.

Another type of transducer that encounters in many IoT systems is an actuator. In simple terms, an actuator operates in the reverse direction of a sensor. It takes an electrical input and turns it into physical action.

![](images/a3.png)

In general, there are many sensors in an IoT embedded systems. They are -
- Temperature Sensor
- Humidity Sensor
- Motion Sensor
- Gas sensor
- Smoke Sensor
- Pressure Sensor
- Image Sensor and many more.

The below picture depicts the types of actuators in embedded systems
![](images/a4.png)

**WORKING OF SENSORS AND ACTUATORS**
![](images/a5.png)

**2. ANALOG AND DIGITAL**

Analog and digital signals are different types which are mainly used to carry the data from one apparatus to another. Analog signals are continuous wave signals that change with time period whereas digital is a discrete signal is a nature. The main difference between analog and digital signals is, analog signals are represented with the sine waves whereas digital signals are represented with square waves. Let us discuss some dissimilarity of analog & digital signals.

![](images/a7.jpg)

Below is the diference between the analog and digital signals.

![](images/a6.png)

**3. MICROPROCESSORS AND MICROCONTROLLERS**

An integrated circuit contained on a single silicon chip, a **microprocessor** contains the arithmetic logic unit, control unit, internal memory registers, and other vital circuitry of a computer's central processing unit (CPU). Microprocessor commonly is used interchangeably with CPU and processor.
A **microcontroller** is a compact integrated circuit designed to govern a specific operation in an embedded system. It is an integrated circuit (IC) device used for controlling other portions of an electronic system, usually via a microprocessor unit (MPU), memory, and some peripherals.

![](images/a8.png)

**4. RASPBERRY Pi**

Raspberry Pi is the name of a series of single-board computers made by the Raspberry Pi Foundation, a UK charity that aims to educate people in computing and create easier access to computing education. It is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.

![](images/a9.jpg)

In general, Raspberry Pi is a
- Mini Computer
- Limited but large power for its size
- No storage
- It is a SOC (System On Chip)
- We can connect shields (Shields - addon functionalities)
- Can connect multiple Pi’s together
- Microprocessor
- Can load a linux OS on it
- Pi uses ARM
- Connect to sensors or actuators

**INTERFACES** - They are the ways to connect a sensor to the microprocessor.

The below picture depicts the interfaces of Raspberry Pi

![](images/a10.png)

**5. PARALLEL AND SERIAL COMMUNICATION**

![](images/a11.jpg)

- PARALLEL INTERFACES - GPIO
- SERIAL INTERFACES - UART, SPI, I2C



